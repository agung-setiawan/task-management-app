## About Laravel

Task management frontend is part of task management project to have function to show or display task list in front end

## How to Install

- Git clone to local: git clone https://agung-setiawan@bitbucket.org/agung-setiawan/task-management-app.git
- Go to folder app.
- Open terminal.
- Go to application folder.
- Execute command : php artisan migrate.
- Execute command : php artisan db:seed --class=UserSeeder.
- Execute command : php artisan serve.
- Open Browser and pointing to URL http://localhost:8000/
- There's 2 type existing user : admin and user.
- Admin login : admin@email.com password : admin_123
- User login : user_1@email.com password : user_123

## Alternative for dump SQL

Otherwise using migration and seeder, you can dump SQL file inside doc folder into MySQL.

## Creator

Agung Setiawan

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
