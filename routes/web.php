<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Auth::routes();

Route::get('/login', [App\Http\Controllers\HomeController::class, 'index'])->name('login');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['prefix' => 'task'], function()
{
    Route::get('index', [App\Http\Controllers\TaskController::class, 'index'])->name('task.index');

    Route::get('create', [App\Http\Controllers\TaskController::class, 'create'])->middleware('can:create-Task');

    Route::post('store', [App\Http\Controllers\TaskController::class, 'store']);

    Route::get('edit/{id}', [App\Http\Controllers\TaskController::class, 'edit'])->middleware('can:edit-Task');

    Route::post('remove', [App\Http\Controllers\TaskController::class, 'remove'])->middleware('can:remove-Task');

    Route::get('v1/get-list', [App\Http\Controllers\TaskController::class, 'taskList']);
});

Route::group(['prefix' => 'user'], function()
{
    Route::get('index', [App\Http\Controllers\UserController::class, 'index'])->name('user.index');

    Route::get('create', [App\Http\Controllers\UserController::class, 'create'])->middleware('can:create-User');

    Route::post('store', [App\Http\Controllers\UserController::class, 'store']);

    Route::get('edit/{id}', [App\Http\Controllers\UserController::class, 'edit'])->middleware('can:edit-User');

    Route::post('update', [App\Http\Controllers\UserController::class, 'update']);

    Route::post('remove', [App\Http\Controllers\UserController::class, 'remove'])->middleware('can:remove-User');
});