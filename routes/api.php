<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::middleware('auth:api')->get('/user', [App\Http\Controllers\UserController::class, 'AuthRouteAPI']);

Route::group(['prefix' => 'v1'], function()
{
    Route::get('task-list', [App\Http\Controllers\TaskController::class, 'taskList']);

});


// Route::get('api/task-list', [App\Http\Controllers\TaskController::class, 'index']function() {
//     // If the Content-Type and Accept headers are set to 'application/json', 
//     // this will return a JSON structure. This will be cleaned up later.
//     return 'Hello';
// });

// Route::get('/api/v1/task-list', function (Request $request) {
//     return 'Hello Worlds';
// });