<?php

namespace App\Http\Controllers;

use Auth;
use Response;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;

use App\Models\Task;
use App\Models\User;

class TaskController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();

        if (Gate::allows('isAdmin')) 
        {
            $task = Task::with('assignee_user')
                        ->orderBy('created_at', 'DESC')
                        ->get()
                        ->toArray();
        } else {
            $task = Task::with('assignee_user')
                        ->where('assignee', $user->id)
                        ->orderBy('created_at', 'DESC')
                        ->get()
                        ->toArray();
        }

        return view('page.task.index')
                ->with('task', $task);
    }

    public function create()
    {
        $user = Auth::user();

        if (Gate::allows('isAdmin')) 
        {
            $users = User::get();
        } else {
            $users = User::where('id', $user->id)->get();
        }

        return view('page.task.add')->with('user', $users);
    }

    public function store(Request $request)
    {
        if($request->method() == 'POST')
        {
            $rules = [
                'title'         => 'required',
                'start_date'    => 'required',
                'start_time'    => 'required',
            ];

            $messages = [];

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails())
            {
                return redirect('task/create')
                        ->with('error', $validator->errors());
            } else {
                if ($request->input('mode') == "create")
                {
                    $data = new Task;
                } else {
                    $data = Task::find($request->input('ids'));
                }

                $data->title        = $request->input('title');
                $data->description  = $request->input('description');
                $data->start_date   = date('Y-m-d', strtotime($request->input('start_date')));
                $data->start_time   = date('H:i:s', strtotime($request->input('start_time')));
                $data->end_date     = (!empty($request->input('end_date')) ? date('Y-m-d', strtotime($request->input('end_date'))) : null);
                $data->end_time     = (!empty($request->input('end_time')) ? date('H:i:s', strtotime($request->input('end_time'))) : null);
                $data->status       = $request->input('status');
                $data->assignee     = $request->input('assignee');

                $data->save();

                return redirect('task/index')->with('success','Data is saved successfully!');
            }
        }
    }

    public function edit($ids = 0)
    {
        if ($ids > 0)
        {
            $q = Task::find($ids);

            if ($q)
            {
                $user = User::get();
                return view('page.task.edit')
                        ->with('data', $q)
                        ->with('user', $user);
            } else {
                return redirect('task/index')->with('error', 'Sorry data not found');
            }
        } else {
            return redirect('task/index')->with('error', 'Error request');
        }
    }

    public function remove(Request $request)
    {
        $q = Task::find($request->input('ids'));

        if ($q)
        {
            $q->delete();

            return Response::json([
                'succes' => true,
                'msg'    => null,
                'date'   => date('d-m-Y H:i:s'),
            ], 200)
            ->withHeaders([
                'Access-Control-Allow-Origin' => '*',
                'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
            ]);
        } else {
            return Response::json([
                'succes' => true,
                'msg'    => 'Record not found',
                'date'   => date('d-m-Y H:i:s'),
            ], 404)
            ->withHeaders([
                'Access-Control-Allow-Origin' => '*',
                'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS'
            ]);
        }
    }

    public function taskList()
    {
        return "Hello Worlds";
    }
}
