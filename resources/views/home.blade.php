@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center mb-3">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Task List') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div>@include('partials/task/_partial_list', ['dataList' => $task, 'swc' => 0])</div>
                </div>
            </div>
        </div>
    </div>

    @can('isAdmin')
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('User List') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div>@include('partials/user/_partial_user_list', ['dataList' => $user, 'swc' => 0])</div>
                    </div>
                </div>
            </div>
        </div>
    @endCan
</div>
@endsection
