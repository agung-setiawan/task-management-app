<table class="tbls table">
    <thead>
        <tr>
            <th>No.</th>
            <th>Name</th>
            <th>Email</th>
            <th>Role</th>
            <th>Register Date</th>
            @if ($swc == 1)
                <th>_Action_</th>
            @endif
        </tr>
    </thead>
    <tbody>
        @php $cnt = 1 @endphp
        @foreach($dataList as $val)
            <tr>
                <td>{{ $cnt }}</td>
                <td>{{ $val['name'] }}</td>
                <td>{{ $val['email'] }}</td>
                <td>{{ $val['role'] }}</td>
                <td>
                    @php
                        $regDate = since($val['created_at']);
                    @endphp
                    {{ $regDate }}
                </td>
                @if ($swc == 1)
                    <td>
                        <div class="dropdown text-center">
                            <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-cogs"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="{{ url('user/edit/' . $val['id']) }}"><i class="fa fa-edit"></i> Edit</a>
                                <a class="dropdown-item" href="javascript:void()" onclick="removeUser({{ $val['id'] }})"><i class="fa fa-trash"></i> Remove</a>
                            </div>
                        </div>
                    </td>
                @endif
            </tr>
            @php $cnt++ @endphp
        @endforeach
    </tbody>
</table>