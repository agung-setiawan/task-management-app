@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center mb-4">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Update User</div>
                <div class="card-body">
                    {{ Form::open(array('url' => 'user/update')) }}
                        <div class="form-group row">
                            <label for="userEmail" class="col-sm-2 col-form-label">Email <em>*</em></label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" name="user_email" value="{{ $data->email }}" id="userEmail" required />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="userName" class="col-sm-2 col-form-label">Name <em>*</em></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="user_name" value="{{ $data->name }}" id="userName" required />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="accessType" class="col-sm-2 col-form-label">Access Type</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="role" id="accessType" required>
                                    <option value="user" {{ ($data->role == 'user') ? 'selected' : null }}>User</option>
                                    <option value="admin" {{ ($data->role == 'admin') ? 'selected' : null }}>Admin</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-2 col-form-label">Password <em>*</em></label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" name="user_password" id="inputPassword" />
                                <div class="small-note">Leave blank if don't want change password</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                @can('isAdmin')
                                    <a href="{{ url('user/index') }}" class="btn btn-md btn-link">Cancel</a>
                                @endcan
                            </div>
                            <div class="col-md-6 text-right">
                                <input type="hidden" name="ids" value="{{ $data['id'] }}" />
                                <button type="submit" class="btn btn-md btn-success">Update</button>
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
