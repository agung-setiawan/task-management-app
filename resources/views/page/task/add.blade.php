@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center mb-4">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create New Task</div>
                <div class="card-body">
                    {{ Form::open(array('url' => 'task/store')) }}
                        <div class="form-group row">
                            <label for="prodName" class="col-sm-2 col-form-label">Title <em>*</em></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="title" value="{{ old('title') }}" id="title" required />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="prodDesc" class="col-sm-2 col-form-label">Description </label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="description" row="5" id="description">{{ old('description') }}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="prodName" class="col-sm-2 col-form-label">Start Date</label>
                            <div class="col-sm-10">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control datepicker" name="start_date" value="{{ old('start_date') }}" required placeholder="pick a date" />
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control start-timepicker" name="start_time" value="{{ old('start_time') }}" required placeholder="pick a time" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="prodName" class="col-sm-2 col-form-label">End Date</label>
                            <div class="col-sm-10">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control datepicker" name="end_date" value="{{ old('end_date') }}" placeholder="pick a date" />
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control end-timepicker" name="end_time" value="{{ old('end_time') }}" placeholder="pick a time" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="prodDesc" class="col-sm-2 col-form-label">Status </label>
                            <div class="col-sm-10">
                                <select class="form-control" name="status">
                                    <option value="PENDING">PENDING</option>
                                    <option value="ONGOING">ON GOING</option>
                                    <option value="COMPLETED">COMPLETED</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="prodDesc" class="col-sm-2 col-form-label">Assignee </label>
                            <div class="col-sm-10">
                                <select class="form-control" name="assignee">
                                    @foreach($user as $val)
                                        <option value="{{ $val->id }}">{{ $val->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <a href="{{ url('task/index') }}" class="btn btn-md btn-link">Cancel</a>
                            </div>
                            <div class="col-md-6 text-right">
                                <input type="hidden" name="mode" value="create">
                                <button type="submit" class="btn btn-md btn-success">Save</button>
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>

<link href="{{ asset('css/bootstrap-datepicker.css') }}" rel="stylesheet">
<link href="{{ asset('css/bootstrap-timepicker.min.css') }}" rel="stylesheet">

<script src="{{ asset('js/bootstrap-datepicker.min.js') }}" defer></script>
<script src="{{ asset('js/bootstrap-timepicker.min.js') }}" defer></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('.datepicker').datepicker({
            format: 'dd-mm-yyyy',
            startDate: '1d',
            autoclose: true
        });
        $('.start-timepicker').timepicker({
            minuteStep: 10,
            showMeridian: false,
            maxHours: 24,
        });
        $('.end-timepicker').timepicker({
            minuteStep: 10,
            showMeridian: false,
            maxHours: 24,
        });
    });
</script>

@endsection