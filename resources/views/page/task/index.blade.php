@extends('layouts.app')

@section('content')
<div class="container">

    <!-- Task -->
    <div class="row justify-content-center mb-3">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Task</div>
                <div class="card-body">
                    <div>@include('partials/task/_partial_list', ['dataList' => $task, 'swc' => 1])</div>
                    <a href="{{ url('task/create') }}" class="btn btn-md btn-primary"><i class="fa fa-plus"></i> Create New Task</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<script type="text/javascript">

    function removeTask(ids)
    {
        var r = confirm('Are you sure remove this task ?');

        if (r == true)
        {
            $.post( "{{ url('/task/remove') }}", {
                "_token": "{{ csrf_token() }}",
                'ids': ids
            }, function( data ) {
                if (data.succes)
                {
                    window.location.reload(true);
                }
            });
        }
    }

</script>
